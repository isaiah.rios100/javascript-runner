package main

import (
	"context"
	"log"
	"os"
	"strconv"

	"github.com/barrydevp/codeatest-runner-core/dispatcher"
	"github.com/barrydevp/codeatest-runner-core/puller"
	"github.com/barrydevp/codeatest-runner-core/runner"
	"github.com/barrydevp/codeatest-runner-core/server"
)

const DEFAULT_PORT = "5001"

var RUNNER_NAME string
var PORT string
var BUCKET_SIZE int64

func init() {
	envPORT := os.Getenv("PORT")

	if envPORT == "" {
		PORT = DEFAULT_PORT
		log.Printf("DEFAULT_PORT %s is set.\n", DEFAULT_PORT)
	} else {
		PORT = envPORT
	}

	RUNNER_NAME = os.Getenv("RUNNER_NAME")
	if RUNNER_NAME == "" {
		RUNNER_NAME = "NodeJs12"
	}

	bucketSize := os.Getenv("CONCURRENCY")

	if bucketSize == "" {
		BUCKET_SIZE = 1
	} else {
		vBucketSize, err := strconv.ParseInt(bucketSize, 10, 64)

		if err != nil {
			log.Println("parse BUCKET_SIZE error, using default BUCKETSIZE = 1")
			BUCKET_SIZE = 1
		} else {
			BUCKET_SIZE = vBucketSize
		}
	}
}

func main() {
	log.Printf("[RUNNER::%s] UP AND RUNNING...\n", RUNNER_NAME)

	ctx := context.Background()

	NodeJSRunner := runner.Runner{
		Name:    RUNNER_NAME,
		State:   "created",
		Command: "node",
	}

	NodeJSPuller := puller.Puller{
		Language:   "js",
		BucketSize: BUCKET_SIZE,
	}

	NodeJSDispatcher := dispatcher.Dispatcher{
		Name:      RUNNER_NAME,
		Runner:    &NodeJSRunner,
		Puller:    &NodeJSPuller,
		Ctx:       ctx,
		IsRunning: false,
		Delay:     10,
	}

	NodeJSDispatcher.Init()

	go NodeJSDispatcher.Run()

	app := server.HttpServer{Dispatcher: &NodeJSDispatcher, PORT: PORT}

	app.ListenAndServe()

	log.Printf("[RUNNER::%s] DOWN BYE BYE...\n", RUNNER_NAME)
}
