# Build go binary
FROM golang:1.14-alpine AS build
LABEL maintainer="barrydevp <barrydevp@gmail.com>"
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o main .

# build production image
FROM node:12-alpine
WORKDIR /app
COPY --from=build /app/main /app/main
COPY --from=build /app/credentials.json /app/credentials.json
EXPOSE 5001
ENV PORT=5001
ENV GO_ENV=production
CMD ["./main"]
